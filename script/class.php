<?php

class ProjectScriptComponent extends CBitrixComponent {

    public function executeComponent() {
        if (defined('STREAM_SOCIALNETWORK_BLOG_POST_EDIT') or CSite::InDir('/bitrix/')) {
            return;
        }
        if ($_POST) {
            if (isset($_POST['action'])) {
                switch ($_POST['action']) {
                    case 'SBPE_get_full_form':
                        $this->includeComponentTemplate();
                        break;

                    default:
                        break;
                }
            }
            return;
        }
        $this->includeComponentTemplate();
    }

}
