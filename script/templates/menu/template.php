<?
$this->SetViewTarget('topblock');
?>
<div id="tm-sonet_log_microblog_container" class="tm-bix_process-create_menu">
    <div id="sonet_log_microblog_container">
        <?
        $arBlogComponentParams = array(
            'ID' => 'new',
            'PATH_TO_BLOG' => '/stream/',
            'PATH_TO_POST' => '/company/personal/user/#user_id#/blog/#post_id#/',
            'PATH_TO_GROUP_POST' => '/workgroups/group/#group_id#/blog/#post_id#/',
            'PATH_TO_POST_EDIT' => '/company/personal/user/#user_id#/blog/edit/#post_id#/',
            'PATH_TO_SMILE' => NULL,
            'SET_TITLE' => 'N',
            'GROUP_ID' => '#BLOG_GROUP_ID#',
            'USER_ID' => cUser::GetId(),
            'SET_NAV_CHAIN' => 'N',
            'USE_SOCNET' => 'N',
            'MICROBLOG' => 'N',
            'USE_CUT' => NULL,
            'NAME_TEMPLATE' => '#LAST_NAME# #NAME#',
            'CHECK_PERMISSIONS_DEST' => 'N',
            'TOP_TABS_VISIBLE' => 'Y',
            'SHOW_BLOG_FORM_TARGET' => false,
            'SOCNET_USER_ID' => NULL,
            'PATH_TO_USER_TASKS' => '/company/personal/user/#user_id#/tasks/',
            'PATH_TO_USER_TASKS_TASK' => '/company/personal/user/#user_id#/tasks/task/#action#/#task_id#/',
            'PATH_TO_GROUP_TASKS' => '/workgroups/group/#group_id#/tasks/',
            'PATH_TO_GROUP_TASKS_TASK' => '/workgroups/group/#group_id#/tasks/task/#action#/#task_id#/',
            'PATH_TO_USER_TASKS_PROJECTS_OVERVIEW' => '/company/personal/user/#user_id#/tasks/projects/',
            'PATH_TO_USER_TEMPLATES_TEMPLATE' => '/company/personal/user/#user_id#/tasks/templates/template/#action#/#template_id#/',
            'LOG_EXPERT_MODE' => 'N',
        );
//        CJSCore::Init(array('fileman', 'viewer', 'socnetlogdest', 'lists', 'tasks_util_query', 'popup', 'comment_aux', 'html_editor'));
        $APPLICATION->IncludeComponent(
                "bitrix:socialnetwork.blog.post.edit", "", $arBlogComponentParams, $component, array("HIDE_ICONS" => "Y")
        );
        ?>
    </div>
</div>
<?
$this->EndViewTarget();
?>