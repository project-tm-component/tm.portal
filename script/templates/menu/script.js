(function (window) {
    'use strict';

    if (window.tmPortalMenu) {
        return;
    }

    window.tmPortalMenu = function () {
        if (window != window.top) {
            window.top.tmPortalMenu();
            $('.slider-panel-close-inner', window.top.document).click();
            return;
        }

        window.SBPEFullForm.getInstance().get({
            callback: function () {
                if (window.BX('tm-sonet_log_microblog_container')) {
                    window.BX('tm-sonet_log_microblog_container').classList.add('tm-bix_process-show');
                }
//                window.SBPETabs.getInstance().showMoreMenu();
//                window.SBPETabs.getInstance().getLists();
                if (window.SBPETabs) {
                    window.SBPETabs.getInstance().setActive('lists', ['80', 'Постановка задач в ТП', null, '<img src="/bitrix/images/lists/default.png" width="36" height="30" border="0" />', null]);
                }
            }
        });
    };

})(window);