<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$arBlog = CBlogPost::GetByID($arResult['BLOG_DATA']['BLOG_POST_ID']);
//$id = 488;
//$user = 710;
$id = $arResult['BLOG_DATA']['BLOG_POST_ID'];
$user = $arBlog['AUTHOR_ID'];
$APPLICATION->IncludeComponent(
        "bitrix:socialnetwork.blog.post", "portal-news", array(
    'CONTENT_NEWS' => $arParams['~CONTENT_NEWS'],
    'PATH_TO_BLOG' => '/news/',
    'PATH_TO_POST' => '/news/',
    'PATH_TO_POST_IMPORTANT' => '/company/personal/user/#user_id#/blog/important/',
    'PATH_TO_BLOG_CATEGORY' => NULL,
    'PATH_TO_POST_EDIT' => '/company/personal/user/#user_id#/blog/edit/#post_id#/',
    'PATH_TO_SEARCH_TAG' => '/search/?tags=#tag#',
    'PATH_TO_USER' => '/company/personal/user/#user_id#/',
    'PATH_TO_GROUP' => '/workgroups/group/#group_id#/',
    'PATH_TO_SMILE' => NULL,
    'PATH_TO_MESSAGES_CHAT' => '/company/personal/messages/chat/#user_id#/',
    'SET_NAV_CHAIN' => 'N',
    'SET_TITLE' => 'N',
    'POST_PROPERTY' => NULL,
    'DATE_TIME_FORMAT' => 'F j, Y h:i a',
    'DATE_TIME_FORMAT_WITHOUT_YEAR' => 'F j, h:i a',
    'TIME_FORMAT' => 'h:i a',
    'CREATED_BY_ID' => false,
    'USER_ID' => $user,
    'ENTITY_TYPE' => 'U',
    'ENTITY_ID' => $user,
    'EVENT_ID' => 'blog_post',
    'EVENT_ID_FULLSET' => 'blog',
    'IND' => '',
    'GROUP_ID' => '#BLOG_GROUP_ID#',
    'SONET_GROUP_ID' => 0,
    'NAME_TEMPLATE' => '#LAST_NAME# #NAME#',
    'SHOW_LOGIN' => 'Y',
    'SHOW_YEAR' => 'M',
    'PATH_TO_CONPANY_DEPARTMENT' => '/company/structure.php?set_filter_structure=Y&structure_UF_DEPARTMENT=#ID#',
    'PATH_TO_VIDEO_CALL' => '/company/personal/video/#user_id#/',
    'USE_SHARE' => NULL,
    'SHARE_HIDE' => NULL,
    'SHARE_TEMPLATE' => NULL,
    'SHARE_HANDLERS' => NULL,
    'SHARE_SHORTEN_URL_LOGIN' => NULL,
    'SHARE_SHORTEN_URL_KEY' => NULL,
    'SHOW_RATING' => 'Y',
    'RATING_TYPE' => 'like',
    'IMAGE_MAX_WIDTH' => NULL,
    'IMAGE_MAX_HEIGHT' => NULL,
    'ALLOW_POST_CODE' => NULL,
    'ID' => $id,
    'LOG_ID' => '0',
    'FROM_LOG' => 'Y',
    'ADIT_MENU' =>
    array(
    ),
    'IS_UNREAD' => true,
    'MARK_NEW_COMMENTS' => 'Y',
    'IS_HIDDEN' => false,
    'LAST_LOG_TS' => 0,
    'CACHE_TIME' => '3600',
    'CACHE_TYPE' => 'A',
    'ALLOW_VIDEO' => NULL,
    'ALLOW_IMAGE_UPLOAD' => NULL,
    'USE_CUT' => NULL,
    'AVATAR_SIZE_COMMON' => 100,
    'AVATAR_SIZE' => 100,
    'AVATAR_SIZE_COMMENT' => 100,
    'LAZYLOAD' => 'Y',
    'CHECK_COMMENTS_PERMS' => 'N',
    'GROUP_READ_ONLY' => 'N',
    'FOLLOW' => 'N',
    'CURRENT_PAGE_DATE' => '',
    'FAVORITES_USER_ID' => 0,
    'CONTENT_ID' => 'BLOG_POST-' . $id,
    'CONTENT_VIEW_CNT' => 0,
        ), false
);
?>
<script>
    BX.ready(function () {
        window.addEventListener("scroll", BX.throttle(function () {
            BX.LazyLoad.onScroll();
        }, 80));
    });
</script>