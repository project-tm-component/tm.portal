<?php

$arParams = array (
    'FORM_ID' => 'blogCommentForm5QdB',
    'SHOW_MORE' => 'Y',
    'PARSER' =>
    array (
      0 => 'Bold',
      1 => 'Italic',
      2 => 'Underline',
      3 => 'Strike',
      4 => 'ForeColor',
      5 => 'FontList',
      6 => 'FontSizeList',
      7 => 'RemoveFormat',
      8 => 'Quote',
      9 => 'Code',
      10 => 'CreateLink',
      11 => 'Image',
      12 => 'UploadImage',
      13 => 'InputVideo',
      14 => 'Table',
      15 => 'Justify',
      16 => 'InsertOrderedList',
      17 => 'InsertUnorderedList',
      18 => 'MentionUser',
      19 => 'SmileList',
      20 => 'Source',
    ),
    'BUTTONS' =>
    array (
      0 => 'UploadFile',
      1 => 'CreateLink',
      2 => 'InputVideo',
      3 => 'Quote',
      4 => 'MentionUser',
    ),
    'TEXT' =>
    array (
      'NAME' => 'comment',
      'VALUE' => '',
      'HEIGHT' => '80px',
    ),
    'DESTINATION' =>
    array (
      'VALUE' =>
      array (
        'LAST' =>
        array (
          'USERS' =>
          array (
            'U739' => 'U739',
            'U742' => 'U742',
            'U710' => 'U710',
            'U75' => 'U75',
            'U448' => 'U448',
            'U624' => 'U624',
            'U568' => 'U568',
            'U221' => 'U221',
            'U434' => 'U434',
            'U716' => 'U716',
            'U482' => 'U482',
          ),
        ),
        'DEPARTMENT' =>
        array (
          'DR1' =>
          array (
            'id' => 'DR1',
            'entityId' => '1',
            'name' => 'Team Market',
            'parent' => 'DR0',
          ),
          'DR39' =>
          array (
            'id' => 'DR39',
            'entityId' => '39',
            'name' => 'Отдел коммерции',
            'parent' => 'DR1',
          ),
          'DR51' =>
          array (
            'id' => 'DR51',
            'entityId' => '51',
            'name' => 'Клиент менеджеры',
            'parent' => 'DR39',
          ),
          'DR4357' =>
          array (
            'id' => 'DR4357',
            'entityId' => '4357',
            'name' => 'Отдел обучения',
            'parent' => 'DR39',
          ),
          'DR993' =>
          array (
            'id' => 'DR993',
            'entityId' => '993',
            'name' => 'Отдел продаж',
            'parent' => 'DR39',
          ),
          'DR4628' =>
          array (
            'id' => 'DR4628',
            'entityId' => '4628',
            'name' => 'Ассистенты интернет-маркетолога',
            'parent' => 'DR993',
          ),
          'DR953' =>
          array (
            'id' => 'DR953',
            'entityId' => '953',
            'name' => 'Привлечение клиентов',
            'parent' => 'DR39',
          ),
          'DR3517' =>
          array (
            'id' => 'DR3517',
            'entityId' => '3517',
            'name' => 'Отдел сопровождения',
            'parent' => 'DR1',
          ),
          'DR4444' =>
          array (
            'id' => 'DR4444',
            'entityId' => '4444',
            'name' => 'Бухгалтерия',
            'parent' => 'DR3517',
          ),
          'DR4448' =>
          array (
            'id' => 'DR4448',
            'entityId' => '4448',
            'name' => 'ИТ-отдел',
            'parent' => 'DR3517',
          ),
          'DR33' =>
          array (
            'id' => 'DR33',
            'entityId' => '33',
            'name' => 'Технический отдел',
            'parent' => 'DR1',
          ),
          'DR35' =>
          array (
            'id' => 'DR35',
            'entityId' => '35',
            'name' => 'SEO',
            'parent' => 'DR33',
          ),
          'DR4426' =>
          array (
            'id' => 'DR4426',
            'entityId' => '4426',
            'name' => 'копирайтинг',
            'parent' => 'DR35',
          ),
          'DR37' =>
          array (
            'id' => 'DR37',
            'entityId' => '37',
            'name' => 'Техническая поддержка',
            'parent' => 'DR33',
          ),
          'DR4508' =>
          array (
            'id' => 'DR4508',
            'entityId' => '4508',
            'name' => 'Чат-боты',
            'parent' => 'DR1',
          ),
        ),
        'DEPARTMENT_RELATION' =>
        array (
        ),
        'EXTRANET_USER' => 'N',
        'USERS' =>
        array (
          'U742' =>
          array (
            'id' => 'U742',
            'entityId' => '742',
            'name' => 'Афонина Любовь',
            'avatar' => '/upload/main/d78/DSC_6744.jpg',
            'desc' => ' ',
            'isExtranet' => 'N',
            'isEmail' => 'N',
            'isCrmEmail' => 'N',
            'checksum' => '3539c1bc3d8df99d3465d220a47baba5',
          ),
          'U739' =>
          array (
            'id' => 'U739',
            'entityId' => '739',
            'name' => 'Баева Анна',
            'avatar' => '/upload/main/039/IMG_6212.JPG',
            'desc' => ' ',
            'isExtranet' => 'N',
            'isEmail' => 'N',
            'isCrmEmail' => 'N',
            'checksum' => '02c5c50d123722ce0600d07d4a5b07e0',
          ),
          'U448' =>
          array (
            'id' => 'U448',
            'entityId' => '448',
            'name' => 'Гринюк Денис',
            'avatar' => '/upload/clouds/1/main/2b6/2b6f5f5cf0d3e79a081e12d55cd424d5/2.jpg',
            'desc' => 'Системный администратор',
            'isExtranet' => 'N',
            'isEmail' => 'N',
            'isCrmEmail' => 'N',
            'checksum' => 'fd8bdbf0a80ddf337838da63356f9434',
          ),
          'U716' =>
          array (
            'id' => 'U716',
            'entityId' => '716',
            'name' => 'Куренкова Алёна',
            'avatar' => '/upload/clouds/1/main/bf1/bf1ad191512ee9d65c64ca9002998fac/IMG_0688-13-04-17-20-21.jpeg',
            'desc' => ' ',
            'isExtranet' => 'N',
            'isEmail' => 'N',
            'isCrmEmail' => 'N',
            'checksum' => 'f1627822c79384d02a32cf98956de601',
          ),
          'U710' =>
          array (
            'id' => 'U710',
            'entityId' => '710',
            'name' => 'Курунов Вячеслав',
            'avatar' => '/upload/clouds/1/main/3ef/3efdc38d65c77088e8addc5e1504bf15/vkurunov.jpg',
            'desc' => 'Руководитель технической поддержки',
            'isExtranet' => 'N',
            'isEmail' => 'N',
            'isCrmEmail' => 'N',
            'checksum' => '24af70544e1d0abca1f852ad0b499127',
          ),
          'U624' =>
          array (
            'id' => 'U624',
            'entityId' => '624',
            'name' => 'Попов Алексей',
            'avatar' => '/upload/clouds/1/main/45a/45afdf5542e3ebd55399652b536f7e85/image.png',
            'desc' => 'программист разработки сайтов',
            'isExtranet' => 'N',
            'isEmail' => 'N',
            'isCrmEmail' => 'N',
            'checksum' => '28c0089ee222e9b4e494c0c191603a39',
          ),
          'U568' =>
          array (
            'id' => 'U568',
            'entityId' => '568',
            'name' => 'Семиволкова Светлана',
            'avatar' => '/upload/clouds/1/main/aa4/aa42f511421c2682380a1bb3c17900c5/IMoPLBpJ5oQ_1_.jpg',
            'desc' => 'программист технической поддержки',
            'isExtranet' => 'N',
            'isEmail' => 'N',
            'isCrmEmail' => 'N',
            'checksum' => '9b72055b2668482cc03855884dbe2975',
          ),
          'U434' =>
          array (
            'id' => 'U434',
            'entityId' => '434',
            'name' => 'Скудин Алексей',
            'avatar' => '/upload/clouds/1/main/c97/c97e7533ad1e087d937183390fdfa302/lexa.jpg',
            'desc' => 'Помощник программиста',
            'isExtranet' => 'N',
            'isEmail' => 'N',
            'isCrmEmail' => 'N',
            'checksum' => '465c332e087361e7bada2adab5ac0ed9',
          ),
          'U75' =>
          array (
            'id' => 'U75',
            'entityId' => '75',
            'name' => 'Федина Елена',
            'avatar' => '/upload/clouds/1/main/b1b/b1bab4f4621c8abcb6d2ddb76d2701e8/Lena.png',
            'desc' => 'Клиент-менеджер (КМ)',
            'isExtranet' => 'N',
            'isEmail' => 'N',
            'isCrmEmail' => 'N',
            'checksum' => '6d2ae8540b63150c45033db7bed84890',
          ),
          'U221' =>
          array (
            'id' => 'U221',
            'entityId' => '221',
            'name' => 'Филатов Артём',
            'avatar' => '/upload/clouds/1/main/c60/c6072cd5f420beee82e4361cebd6def1/0cbae574c418c1218a610da8a64d2c2e.jpg',
            'desc' => 'Программист технической поддержки',
            'isExtranet' => 'N',
            'isEmail' => 'N',
            'isCrmEmail' => 'N',
            'checksum' => '06bbc6bb3735222bf1249ef09545cfb6',
          ),
        ),
      ),
      'SHOW' => 'N',
      'USE_CLIENT_DATABASE' => 'Y',
    ),
    'UPLOAD_FILE' => false,
    'UPLOAD_WEBDAV_ELEMENT' =>
    array (
      'ID' => '49',
      'ENTITY_ID' => 'BLOG_COMMENT',
      'FIELD_NAME' => 'UF_BLOG_COMMENT_FILE',
      'USER_TYPE_ID' => 'disk_file',
      'XML_ID' => 'UF_BLOG_COMMENT_FILE',
      'SORT' => '100',
      'MULTIPLE' => 'Y',
      'MANDATORY' => 'N',
      'SHOW_FILTER' => 'N',
      'SHOW_IN_LIST' => 'N',
      'EDIT_IN_LIST' => 'Y',
      'IS_SEARCHABLE' => 'Y',
      'SETTINGS' =>
      array (
        'IBLOCK_ID' => 0,
        'SECTION_ID' => 0,
        'UF_TO_SAVE_ALLOW_EDIT' => NULL,
      ),
      'EDIT_FORM_LABEL' => 'UF_BLOG_COMMENT_FILE',
      'LIST_COLUMN_LABEL' => NULL,
      'LIST_FILTER_LABEL' => NULL,
      'ERROR_MESSAGE' => NULL,
      'HELP_MESSAGE' => NULL,
      'USER_TYPE' =>
      array (
        'USER_TYPE_ID' => 'disk_file',
        'CLASS_NAME' => 'Bitrix\\Disk\\Uf\\FileUserType',
        'DESCRIPTION' => 'Файл (Диск)',
        'BASE_TYPE' => 'int',
        'TAG' =>
        array (
          0 => 'DISK FILE ID',
          1 => 'DOCUMENT ID',
        ),
      ),
      'VALUE' => false,
      '~EDIT_FORM_LABEL' => 'UF_BLOG_COMMENT_FILE',
    ),
    'UPLOAD_FILE_PARAMS' =>
    array (
      'width' => 400,
      'height' => 400,
    ),
    'FILES' =>
    array (
      'VALUE' =>
      array (
      ),
      'DEL_LINK' => NULL,
      'SHOW' => 'N',
      'POSTFIX' => 'file',
    ),
    'SMILES' => 1,
    'LHE' =>
    array (
      'documentCSS' => 'body {color:#434343;}',
      'ctrlEnterHandler' => '__submit5QdB',
      'id' => 'idLHE_blogCommentForm5QdB',
      'fontFamily' => '\'Helvetica Neue\', Helvetica, Arial, sans-serif',
      'fontSize' => '12px',
      'bInitByJS' => true,
      'height' => 80,
    ),
    'IS_BLOG' => true,
    'PROPERTIES' =>
    array (
      0 =>
      array (
        'ID' => '220',
        'ENTITY_ID' => 'BLOG_COMMENT',
        'FIELD_NAME' => 'UF_BLOG_COMM_URL_PRV',
        'USER_TYPE_ID' => 'url_preview',
        'XML_ID' => 'UF_BLOG_COMM_URL_PRV',
        'SORT' => '100',
        'MULTIPLE' => 'N',
        'MANDATORY' => 'N',
        'SHOW_FILTER' => 'N',
        'SHOW_IN_LIST' => 'N',
        'EDIT_IN_LIST' => 'Y',
        'IS_SEARCHABLE' => 'Y',
        'SETTINGS' =>
        array (
        ),
        'EDIT_FORM_LABEL' => 'UF_BLOG_COMM_URL_PRV',
        'LIST_COLUMN_LABEL' => NULL,
        'LIST_FILTER_LABEL' => NULL,
        'ERROR_MESSAGE' => NULL,
        'HELP_MESSAGE' => NULL,
        'USER_TYPE' =>
        array (
          'USER_TYPE_ID' => 'url_preview',
          'CLASS_NAME' => 'Bitrix\\Main\\UrlPreview\\UrlPreviewUserType',
          'DESCRIPTION' => 'Содержимое ссылки',
          'BASE_TYPE' => 'int',
        ),
        'VALUE' => false,
        '~EDIT_FORM_LABEL' => 'UF_BLOG_COMM_URL_PRV',
        'ELEMENT_ID' => 'url_preview_5QdB',
      ),
    ),
    'DISABLE_LOCAL_EDIT' => false,
    'CACHE_TYPE' => 'A',
    '~FORM_ID' => 'blogCommentForm5QdB',
    '~SHOW_MORE' => 'Y',
    '~PARSER' =>
    array (
      0 => 'Bold',
      1 => 'Italic',
      2 => 'Underline',
      3 => 'Strike',
      4 => 'ForeColor',
      5 => 'FontList',
      6 => 'FontSizeList',
      7 => 'RemoveFormat',
      8 => 'Quote',
      9 => 'Code',
      10 => 'CreateLink',
      11 => 'Image',
      12 => 'UploadImage',
      13 => 'InputVideo',
      14 => 'Table',
      15 => 'Justify',
      16 => 'InsertOrderedList',
      17 => 'InsertUnorderedList',
      18 => 'MentionUser',
      19 => 'SmileList',
      20 => 'Source',
    ),
    '~BUTTONS' =>
    array (
      0 => 'UploadFile',
      1 => 'CreateLink',
      2 => 'InputVideo',
      3 => 'Quote',
      4 => 'MentionUser',
    ),
    '~TEXT' =>
    array (
      'NAME' => 'comment',
      'VALUE' => '',
      'HEIGHT' => '80px',
    ),
    '~DESTINATION' =>
    array (
      'VALUE' =>
      array (
        'LAST' =>
        array (
          'USERS' =>
          array (
            'U739' => 'U739',
            'U742' => 'U742',
            'U710' => 'U710',
            'U75' => 'U75',
            'U448' => 'U448',
            'U624' => 'U624',
            'U568' => 'U568',
            'U221' => 'U221',
            'U434' => 'U434',
            'U716' => 'U716',
            'U482' => 'U482',
          ),
        ),
        'DEPARTMENT' =>
        array (
          'DR1' =>
          array (
            'id' => 'DR1',
            'entityId' => '1',
            'name' => 'Team Market',
            'parent' => 'DR0',
          ),
          'DR39' =>
          array (
            'id' => 'DR39',
            'entityId' => '39',
            'name' => 'Отдел коммерции',
            'parent' => 'DR1',
          ),
          'DR51' =>
          array (
            'id' => 'DR51',
            'entityId' => '51',
            'name' => 'Клиент менеджеры',
            'parent' => 'DR39',
          ),
          'DR4357' =>
          array (
            'id' => 'DR4357',
            'entityId' => '4357',
            'name' => 'Отдел обучения',
            'parent' => 'DR39',
          ),
          'DR993' =>
          array (
            'id' => 'DR993',
            'entityId' => '993',
            'name' => 'Отдел продаж',
            'parent' => 'DR39',
          ),
          'DR4628' =>
          array (
            'id' => 'DR4628',
            'entityId' => '4628',
            'name' => 'Ассистенты интернет-маркетолога',
            'parent' => 'DR993',
          ),
          'DR953' =>
          array (
            'id' => 'DR953',
            'entityId' => '953',
            'name' => 'Привлечение клиентов',
            'parent' => 'DR39',
          ),
          'DR3517' =>
          array (
            'id' => 'DR3517',
            'entityId' => '3517',
            'name' => 'Отдел сопровождения',
            'parent' => 'DR1',
          ),
          'DR4444' =>
          array (
            'id' => 'DR4444',
            'entityId' => '4444',
            'name' => 'Бухгалтерия',
            'parent' => 'DR3517',
          ),
          'DR4448' =>
          array (
            'id' => 'DR4448',
            'entityId' => '4448',
            'name' => 'ИТ-отдел',
            'parent' => 'DR3517',
          ),
          'DR33' =>
          array (
            'id' => 'DR33',
            'entityId' => '33',
            'name' => 'Технический отдел',
            'parent' => 'DR1',
          ),
          'DR35' =>
          array (
            'id' => 'DR35',
            'entityId' => '35',
            'name' => 'SEO',
            'parent' => 'DR33',
          ),
          'DR4426' =>
          array (
            'id' => 'DR4426',
            'entityId' => '4426',
            'name' => 'копирайтинг',
            'parent' => 'DR35',
          ),
          'DR37' =>
          array (
            'id' => 'DR37',
            'entityId' => '37',
            'name' => 'Техническая поддержка',
            'parent' => 'DR33',
          ),
          'DR4508' =>
          array (
            'id' => 'DR4508',
            'entityId' => '4508',
            'name' => 'Чат-боты',
            'parent' => 'DR1',
          ),
        ),
        'DEPARTMENT_RELATION' =>
        array (
        ),
        'EXTRANET_USER' => 'N',
        'USERS' =>
        array (
          'U742' =>
          array (
            'id' => 'U742',
            'entityId' => '742',
            'name' => 'Афонина Любовь',
            'avatar' => '/upload/main/d78/DSC_6744.jpg',
            'desc' => ' ',
            'isExtranet' => 'N',
            'isEmail' => 'N',
            'isCrmEmail' => 'N',
            'checksum' => '3539c1bc3d8df99d3465d220a47baba5',
          ),
          'U739' =>
          array (
            'id' => 'U739',
            'entityId' => '739',
            'name' => 'Баева Анна',
            'avatar' => '/upload/main/039/IMG_6212.JPG',
            'desc' => ' ',
            'isExtranet' => 'N',
            'isEmail' => 'N',
            'isCrmEmail' => 'N',
            'checksum' => '02c5c50d123722ce0600d07d4a5b07e0',
          ),
          'U448' =>
          array (
            'id' => 'U448',
            'entityId' => '448',
            'name' => 'Гринюк Денис',
            'avatar' => '/upload/clouds/1/main/2b6/2b6f5f5cf0d3e79a081e12d55cd424d5/2.jpg',
            'desc' => 'Системный администратор',
            'isExtranet' => 'N',
            'isEmail' => 'N',
            'isCrmEmail' => 'N',
            'checksum' => 'fd8bdbf0a80ddf337838da63356f9434',
          ),
          'U716' =>
          array (
            'id' => 'U716',
            'entityId' => '716',
            'name' => 'Куренкова Алёна',
            'avatar' => '/upload/clouds/1/main/bf1/bf1ad191512ee9d65c64ca9002998fac/IMG_0688-13-04-17-20-21.jpeg',
            'desc' => ' ',
            'isExtranet' => 'N',
            'isEmail' => 'N',
            'isCrmEmail' => 'N',
            'checksum' => 'f1627822c79384d02a32cf98956de601',
          ),
          'U710' =>
          array (
            'id' => 'U710',
            'entityId' => '710',
            'name' => 'Курунов Вячеслав',
            'avatar' => '/upload/clouds/1/main/3ef/3efdc38d65c77088e8addc5e1504bf15/vkurunov.jpg',
            'desc' => 'Руководитель технической поддержки',
            'isExtranet' => 'N',
            'isEmail' => 'N',
            'isCrmEmail' => 'N',
            'checksum' => '24af70544e1d0abca1f852ad0b499127',
          ),
          'U624' =>
          array (
            'id' => 'U624',
            'entityId' => '624',
            'name' => 'Попов Алексей',
            'avatar' => '/upload/clouds/1/main/45a/45afdf5542e3ebd55399652b536f7e85/image.png',
            'desc' => 'программист разработки сайтов',
            'isExtranet' => 'N',
            'isEmail' => 'N',
            'isCrmEmail' => 'N',
            'checksum' => '28c0089ee222e9b4e494c0c191603a39',
          ),
          'U568' =>
          array (
            'id' => 'U568',
            'entityId' => '568',
            'name' => 'Семиволкова Светлана',
            'avatar' => '/upload/clouds/1/main/aa4/aa42f511421c2682380a1bb3c17900c5/IMoPLBpJ5oQ_1_.jpg',
            'desc' => 'программист технической поддержки',
            'isExtranet' => 'N',
            'isEmail' => 'N',
            'isCrmEmail' => 'N',
            'checksum' => '9b72055b2668482cc03855884dbe2975',
          ),
          'U434' =>
          array (
            'id' => 'U434',
            'entityId' => '434',
            'name' => 'Скудин Алексей',
            'avatar' => '/upload/clouds/1/main/c97/c97e7533ad1e087d937183390fdfa302/lexa.jpg',
            'desc' => 'Помощник программиста',
            'isExtranet' => 'N',
            'isEmail' => 'N',
            'isCrmEmail' => 'N',
            'checksum' => '465c332e087361e7bada2adab5ac0ed9',
          ),
          'U75' =>
          array (
            'id' => 'U75',
            'entityId' => '75',
            'name' => 'Федина Елена',
            'avatar' => '/upload/clouds/1/main/b1b/b1bab4f4621c8abcb6d2ddb76d2701e8/Lena.png',
            'desc' => 'Клиент-менеджер (КМ)',
            'isExtranet' => 'N',
            'isEmail' => 'N',
            'isCrmEmail' => 'N',
            'checksum' => '6d2ae8540b63150c45033db7bed84890',
          ),
          'U221' =>
          array (
            'id' => 'U221',
            'entityId' => '221',
            'name' => 'Филатов Артём',
            'avatar' => '/upload/clouds/1/main/c60/c6072cd5f420beee82e4361cebd6def1/0cbae574c418c1218a610da8a64d2c2e.jpg',
            'desc' => 'Программист технической поддержки',
            'isExtranet' => 'N',
            'isEmail' => 'N',
            'isCrmEmail' => 'N',
            'checksum' => '06bbc6bb3735222bf1249ef09545cfb6',
          ),
        ),
      ),
      'SHOW' => 'N',
      'USE_CLIENT_DATABASE' => 'Y',
    ),
    '~UPLOAD_FILE' => false,
    '~UPLOAD_WEBDAV_ELEMENT' =>
    array (
      'ID' => '49',
      'ENTITY_ID' => 'BLOG_COMMENT',
      'FIELD_NAME' => 'UF_BLOG_COMMENT_FILE',
      'USER_TYPE_ID' => 'disk_file',
      'XML_ID' => 'UF_BLOG_COMMENT_FILE',
      'SORT' => '100',
      'MULTIPLE' => 'Y',
      'MANDATORY' => 'N',
      'SHOW_FILTER' => 'N',
      'SHOW_IN_LIST' => 'N',
      'EDIT_IN_LIST' => 'Y',
      'IS_SEARCHABLE' => 'Y',
      'SETTINGS' =>
      array (
        'IBLOCK_ID' => 0,
        'SECTION_ID' => 0,
        'UF_TO_SAVE_ALLOW_EDIT' => NULL,
      ),
      'EDIT_FORM_LABEL' => 'UF_BLOG_COMMENT_FILE',
      'LIST_COLUMN_LABEL' => NULL,
      'LIST_FILTER_LABEL' => NULL,
      'ERROR_MESSAGE' => NULL,
      'HELP_MESSAGE' => NULL,
      'USER_TYPE' =>
      array (
        'USER_TYPE_ID' => 'disk_file',
        'CLASS_NAME' => 'Bitrix\\Disk\\Uf\\FileUserType',
        'DESCRIPTION' => 'Файл (Диск)',
        'BASE_TYPE' => 'int',
        'TAG' =>
        array (
          0 => 'DISK FILE ID',
          1 => 'DOCUMENT ID',
        ),
      ),
      'VALUE' => false,
      '~EDIT_FORM_LABEL' => 'UF_BLOG_COMMENT_FILE',
    ),
    '~UPLOAD_FILE_PARAMS' =>
    array (
      'width' => 400,
      'height' => 400,
    ),
    '~FILES' =>
    array (
      'VALUE' =>
      array (
      ),
      'DEL_LINK' => NULL,
      'SHOW' => 'N',
      'POSTFIX' => 'file',
    ),
    '~SMILES' => 1,
    '~LHE' =>
    array (
      'documentCSS' => 'body {color:#434343;}',
      'ctrlEnterHandler' => '__submit5QdB',
      'id' => 'idLHE_blogCommentForm5QdB',
      'fontFamily' => '\'Helvetica Neue\', Helvetica, Arial, sans-serif',
      'fontSize' => '12px',
      'bInitByJS' => true,
      'height' => 80,
    ),
    '~IS_BLOG' => true,
    '~PROPERTIES' =>
    array (
      0 =>
      array (
        'ID' => '220',
        'ENTITY_ID' => 'BLOG_COMMENT',
        'FIELD_NAME' => 'UF_BLOG_COMM_URL_PRV',
        'USER_TYPE_ID' => 'url_preview',
        'XML_ID' => 'UF_BLOG_COMM_URL_PRV',
        'SORT' => '100',
        'MULTIPLE' => 'N',
        'MANDATORY' => 'N',
        'SHOW_FILTER' => 'N',
        'SHOW_IN_LIST' => 'N',
        'EDIT_IN_LIST' => 'Y',
        'IS_SEARCHABLE' => 'Y',
        'SETTINGS' =>
        array (
        ),
        'EDIT_FORM_LABEL' => 'UF_BLOG_COMM_URL_PRV',
        'LIST_COLUMN_LABEL' => NULL,
        'LIST_FILTER_LABEL' => NULL,
        'ERROR_MESSAGE' => NULL,
        'HELP_MESSAGE' => NULL,
        'USER_TYPE' =>
        array (
          'USER_TYPE_ID' => 'url_preview',
          'CLASS_NAME' => 'Bitrix\\Main\\UrlPreview\\UrlPreviewUserType',
          'DESCRIPTION' => 'Содержимое ссылки',
          'BASE_TYPE' => 'int',
        ),
        'VALUE' => false,
        '~EDIT_FORM_LABEL' => 'UF_BLOG_COMM_URL_PRV',
        'ELEMENT_ID' => 'url_preview_5QdB',
      ),
    ),
    '~DISABLE_LOCAL_EDIT' => false,
    '~CACHE_TYPE' => 'A',
    'NAME_TEMPLATE' => '#LAST_NAME# #NAME#',
  );
