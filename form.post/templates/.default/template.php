<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
include_once($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/functions.php");
include_once($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/message.php");
include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/file.php");
include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/urlpreview.php");

$this->addExternalCss('/bitrix/components/bitrix/main.post.form/templates/.default/style.min.css');
$this->addExternalJs('/bitrix/components/bitrix/main.post.form/templates/.default/script.js');

CUtil::InitJSCore(array('socnetlogdest', 'fx'));
foreach ($arParams["BUTTONS"] as $val) {
    switch ($val) {
        case "CreateLink":
            $arButtonsHTML[] = '<span class="feed-add-post-form-but-cnt" id="bx-b-link-' . $arParams["FORM_ID"] . '"></span>';
            break;
        case "UploadImage":
        case "UploadFile":
            $arButtonsHTML["Upload"] = '<span class="feed-add-post-form-but feed-add-file" id="bx-b-uploadfile-' . $arParams["FORM_ID"] . '" ' .
                    'title="' . GetMessage('MPF_FILE_TITLE') . '"></span>';
            break;
        case "InputVideo":
            $arButtonsHTML[] = '<span class="feed-add-post-form-but-cnt" id="bx-b-video-' . $arParams["FORM_ID"] . '"></span>';
            break;
        case "InputTag":
            $arButtonsHTML[] = '<span class="feed-add-post-form-but feed-add-tag" id="bx-b-tag-input-' . $arParams["FORM_ID"] . '" ' .
                    'title="' . GetMessage("MPF_TAG_TITLE") . '"></span>';
            break;
        case "MentionUser":
            $arButtonsHTML[] = '<span class="feed-add-post-form-but feed-add-mention" id="bx-b-mention-' . $arParams["FORM_ID"] . '" ' .
                    'title="' . GetMessage("MPF_MENTION_TITLE") . '"></span>';
            break;
        case "Quote":
            $arButtonsHTML[] = '<span class="feed-add-post-form-but-cnt" id="bx-b-quote-' . $arParams["FORM_ID"] . '"></span>';
            break;
        case "Important":
            $arButtonsHTML[] = '<span class="feed-add-post-form-but feed-add-important" id="bx-b-important-' . $arParams["FORM_ID"] . '" ' .
                    'title="' . GetMessage("MPF_IMPORTANT_TITLE") . '"></span>' .
                    '<span id="bx-b-important-' . $arParams["FORM_ID"] . '-active" class="feed-add-important-active-block" style="display: none;"><span class="feed-add-post-form-but feed-add-important-active"></span><span class="feed-add-important-text">' . GetMessage('MPF_IMPORTANT_TITLE') . '</span></span>';
            break;
        default:
            if (isset($arParams["~BUTTONS_HTML"]) && is_array($arParams["~BUTTONS_HTML"]) && is_array($arParams["~BUTTONS_HTML"]) && array_key_exists($val, $arParams["~BUTTONS_HTML"]))
                $arButtonsHTML[] = $arParams["~BUTTONS_HTML"][$val];
            break;
    }
}
?>
<div class="feed-add-post" id="div<?= $arParams["divId"] ?>" <? if ($arParams["LHE"]["lazyLoad"]): ?> style="display:none;"<? endif; ?>><? ?><div class="feed-add-post-dnd">
        <div class="feed-add-post-dnd-inner">
            <span class="feed-add-post-dnd-icon"></span>
            <span class="feed-add-post-dnd-text"><?= GetMessage("MPF_SELECT_ATTACHMENTS") ?><span><?= GetMessage("MPF_DROP_ATTACHMENTS") ?></span></span>
        </div>
    </div><? ?><div class="feed-add-post-dnd-notice">
        <div class="feed-add-post-dnd-inner">
            <span class="feed-add-post-dnd-icon"></span>
            <span class="feed-add-post-dnd-text"><?= GetMessage("MPF_DRAG_ATTACHMENTS") ?></span>
        </div>
    </div><?
?><div class="feed-add-post-form feed-add-post-edit-form">
    <?= $arParams["~HTML_BEFORE_TEXTAREA"] ?>
        <div class="feed-add-post-text">
            <script type="text/javascript">
<?
if (is_array($GLOBALS["arExtranetGroupID"])) {
    ?>
                    if (typeof window['arExtranetGroupID'] == 'undefined')
                    {
                        window['arExtranetGroupID'] = <?= CUtil::PhpToJSObject($GLOBALS["arExtranetGroupID"]) ?>;
                    }
    <?
}
?>
                BX.ready(function ()
                {
                    if (!LHEPostForm.getHandler('<?= $arParams["LHE"]["id"] ?>'))
                    {
<? if ($arParams["JS_OBJECT_NAME"] !== ""): ?>window['<?= $arParams["JS_OBJECT_NAME"] ?>'] = <? endif; ?>new LHEPostForm(
                                '<?= $arParams["FORM_ID"] ?>',
<?=
CUtil::PhpToJSObject(
        array(
            "LHEJsObjId" => $arParams["LHE"]["id"],
            "LHEJsObjName" => $arParams["LHE"]["jsObjName"],
            "arSize" => $arParams["UPLOAD_FILE_PARAMS"],
            "CID" => $arParams["UPLOADS_CID"],
            'parsers' => $arParams["PARSER"],
            'showPanelEditor' => ($arParams["TEXT"]["SHOW"] == "Y"),
            'pinEditorPanel' => ($arParams["PIN_EDITOR_PANEL"] == "Y"),
            'formID' => $arParams["FORM_ID"],
            'lazyLoad' => !!$arParams["LHE"]['lazyLoad'],
            'ctrlEnterHandler' => $arParams["LHE"]['ctrlEnterHandler'],
            'urlPreviewId' => $arParams['urlPreviewId']
));
?>
                        );
                    } else
                    {
                        BX.debug('LHEPostForm <?= $arParams["LHE"]["id"] ?> has already existed.');
                    }
                });
            </script>
            <?
            include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/lhe.php");
            ?>
            <div style="display:none;"><input type="text" tabindex="<?= ($arParams["TEXT"]["TABINDEX"] ++) ?>" onFocus="LHEPostForm.getEditor('<?= $arParams["LHE"]["id"] ?>').SetFocus()" name="hidden_focus" /></div>
        </div>
        <div class="feed-add-post-form-but-wrap" id="post-buttons-bottom"><?=
            implode("", $arButtonsHTML);
            if (!empty($arParams["ADDITIONAL"])) {
                if ($arParams["ADDITIONAL_TYPE"] == "popup") {
                    ?><div class="feed-add-post-form-but-more" <?
                    ?>onclick="BX.PopupMenu.show('menu-more<?= $arParams["FORM_ID"] ?>', this, [<?= implode(", ", $arParams["ADDITIONAL"]) ?>], {offsetLeft: 42, offsetTop: 3, lightShadow: false, angle: top, events: {onPopupClose: function (popupWindow) {
                                                BX.removeClass(this.bindElement, 'feed-add-post-form-but-more-act');
                                            }}});
                                    BX.addClass(this, 'feed-add-post-form-but-more-act');"><?
                    ?><?= GetMessage("MPF_MORE") ?><?
                    ?><div class="feed-add-post-form-but-arrow"></div><?
                    ?></div><?
                } else if (count($arParams["ADDITIONAL"]) < 5) {
                    ?><div class="feed-add-post-form-but-more-open"><?
                    ?><?= implode("", $arParams["ADDITIONAL"]) ?>
                    </div><?
                } else {
                    foreach ($arParams["ADDITIONAL"] as $key => $val) {
                        $arParams["ADDITIONAL"][$key] = array("text" => $val, "onclick" => "BX.PopupMenu.Data['menu-more" . $arParams["FORM_ID"] . "'].popupWindow.close();");
                    }
                    ?><script type="text/javascript">window['more<?= $arParams["FORM_ID"] ?>'] =<?= CUtil::PhpToJSObject($arParams["ADDITIONAL"]) ?>;</script><?
                    ?><div class="feed-add-post-form-but-more" <?
                    ?>onclick="BX.PopupMenu.show('menu-more<?= $arParams["FORM_ID"] ?>', this, window['more<?= $arParams["FORM_ID"] ?>'], {offsetLeft: 42, offsetTop: 3, lightShadow: false, angle: top, events: {onPopupClose: function (popupWindow) {
                                                BX.removeClass(this.bindElement, 'feed-add-post-form-but-more-act');
                                            }}});
                                    BX.addClass(this, 'feed-add-post-form-but-more-act');"><?
                    ?><?= GetMessage("MPF_MORE") ?><?
                    ?><div class="feed-add-post-form-but-arrow"></div><?
                    ?></div><?
                    }
                }
                ?></div>

    </div>
    <?= $arParams["~HTML_AFTER_TEXTAREA"] ?><?
    ?><?= $arParams["UPLOADS_HTML"] ?><?
    ?><?= $arParams["~AT_THE_END_HTML"] ?><?
    ?><?= $arParams["URL_PREVIEW_HTML"] ?><?
